# Setup

> VSC setup.



## Online REPL


https://scastie.scala-lang.org/?target=dotty



## Visual Studio Code Extension

Scala (sbt)




## Ubuntu

echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt update
sudo apt install sbt

sbt sbtVersion
sbt about


https://github.com/lampepfl/dotty/releases




## Mac


brew install lampepfl/brew/dotty
brew upgrade dotty


## Test 

sbt new lampepfl/dotty.g8
sbt new lampepfl/dotty-cross.g8

sbt
run
~run


dotc Main.scala
dotr Main


sbt launchIDE





## References




https://dotty.epfl.ch/docs/usage/getting-started.html

https://dotty.epfl.ch/docs/usage/ide-support.html

https://dotty.epfl.ch/docs/usage/worksheet-mode.html




